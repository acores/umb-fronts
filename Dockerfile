FROM ubuntu:22.04

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt clean && apt update && apt -y install apt-transport-https wget tar npm git curl build-essential checkinstall python3-tk

RUN apt -y install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev openssl

RUN wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" https://javadl.oracle.com/webapps/download/GetFile/1.8.0_271-b09/61ae65e088624f5aaa0b1d2d801acb16/linux-i586/jdk-8u271-linux-x64.tar.gz && \
        mkdir -p /opt/jvm && \
        cd /opt/jvm && \
        tar -xvzf /jdk-8u271-linux-x64.tar.gz && \
        echo -e '\
        PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/jvm/jdk1.8.0_271/bin:/opt/jvm/jdk1.8.0_271/db/bin:/opt/jvm/jdk1.8.0_271/jre/bin"\n\
        J2SDKDIR="/opt/jvm/jdk1.8.0_271"\n\
        J2REDIR="/opt/jvm/jdk1.8.0_271/jre"\n\
        JAVA_HOME="/opt/jvm/jdk1.8.0_271"\n\
        DERBY_HOME="/opt/jvm/jdk1.8.0_271/db"\n\
        J2SDKDIR="/opt/jvm/jdk1.8.0_271"\n\
        J2REDIR="/opt/jvm/jdk1.8.0_271/jre*"\n\
        JAVA_HOME="/opt/jvm/jdk1.8.0_271"\n\
        DERBY_HOME="/opt/jvm/jdk1.8.0_271/db"\
        ' > /etc/environment && \
        update-alternatives --install "/usr/bin/java" "java" "/opt/jvm/jdk1.8.0_271/bin/java" 0 && \
        update-alternatives --install "/usr/bin/javac" "javac" "/opt/jvm/jdk1.8.0_271/bin/javac" 0 && \
        update-alternatives --set java /opt/jvm/jdk1.8.0_271/bin/java && \
        update-alternatives --set javac /opt/jvm/jdk1.8.0_271/bin/javac && \
        update-alternatives --list java && \
        update-alternatives --list javac && \
        java -version && \
        apt update && \
        apt install maven -y && \
        python3 -m pip install j2cli && \
        npm install -g @angular/cli && \
        npm install pm2 -g && \
        cd /opt/  && \
        mkdir /opt/nvm && \
        wget https://www.python.org/ftp/python/3.9.12/Python-3.9.12.tgz && \
	    tar xzf Python-3.9.12.tgz && \
	    cd Python-3.9.12 && \
	    ./configure --prefix=/home/user/sources/compiled/python3.9_dev --with-ensurepip=install && sleep 4 && \
	    ./configure --enable-optimizations && \
	    make && \
	    make install && \
        git config --global http.sslverify "false"

ENV NVM_DIR /opt/nvm
ENV NODE_VERSION v18.16.1
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default \
    && apt remove nodejs -y \
    && cp /usr/local/bin/python3.9* /usr/bin/ && cp /usr/local/bin/pip3.9* /usr/bin/ \
	&& update-alternatives --install /usr/bin/python python /usr/bin/python3.9 1 \
	&& update-alternatives --config python \
	&& update-alternatives --install /usr/bin/pip pip /usr/bin/pip3.9 1 \
	&& update-alternatives --config pip \
	&& python -V \
	&& pip -V

ENV NODE_PATH $NVM_DIR/$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH
